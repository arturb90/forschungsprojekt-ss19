\chapter{Fundamentals}\markboth{Fundamentals}{Fundamentals}%\addcontentsline{toc}{chapter}{Fundamentals}

This section presents basic concepts and terminology relating to dialogue systems. The focus will be laid on task-oriented dialogue systems, which are specialized in solving a particular task, such as ordering food, rather than non-task-oriented systems. Traditional pipeline-based approaches are contrasted with more recent end-to-end techniques for building dialogue systems. Finally, the state of the art in deep learning approaches for sequence modeling and prediction is examined, since both pipeline-based and end-to-end approaches depend heavily on those techniques.

\section{Task-Oriented Dialogue Systems}

As the name indicates, task-oriented dialogue systems are conversational agents that are geared towards solving a particular task within a particular domain, such as booking services or making appointments. Thus, they operate on a very limited scope which allows for partially structured conversation templates, or \textit{user stories} (see figure \colorbox{yellow}{[FIGURE][CITE]}). User stories represent an abstracted description of the conversation flow between users and conversational agents. The conversation flow is determined by the goal of the user and the associated constraints. For example, when users want to book a restaurant, they might prefer a restaurant in a certain price range and a specific type of cuisine. Relevant constraints to the request may not be mentioned and must be obtained on the agent's side by requesting clarification \colorbox{yellow}{[CITE]}. This is exemplified in figure \ref{fig:caconv}: Since the preferred type of cuisine was not specified in the user request, the dialogue system attempts to retrieve that particular piece of information. Given such a problem description, the objective of task-oriented dialogue systems may be characterized as obtaining all relevant user constraints with as few dialogue turns as possible and returning the best match result. \medbreak

\begin{table}[!htbp]
	\begin{center}
			\begin{tabularx}{0.63\textwidth}{ c | c }
				\textbf{Slot} & \textbf{Value} \\ \hline \hline
				\texttt{departure} & BER, MUC, HAM, ... \\ \hline
				\texttt{destination} & BER, MUC, HAM, ... \\ \hline
				\texttt{airline} & Lufthansa, Emirates, American, ... \\ \hline
				\texttt{travelClass} & economy, business, first \\ \hline
				\texttt{directFlight} & yes, no \\ \hline
				... & ... 
			\end{tabularx}
	\end{center}
	\vspace{-15pt}
	\caption{Terms that are related and constitute the domain of flight booking.} \label{tab:dom}
\end{table}

The target domain for a task-oriented dialogue system is determined by a set of ontologies, providing formal specifications of relevant domains, domain terms and the relationships among them. These specifications are usually established in terms of the theory of \textbf{frame semantics} \colorbox{yellow}{[CITE]}. It asserts that the meaning of a natural language utterance has to be evaluated within a specific frame, which is associated with a unique set of concepts (see table \ref{tab:dom}). For example, the domain of flight booking invokes a wide range of terms, which are usually referred to as \textit{slots}, that are related to it and which are the very elements that define what it really means to book a flight. This is referred to as \textit{semantic} or \textit{slot frame}. Each element of a slot frame may refer to another slot frame itself. The frame invoked by an user utterance is also referred to as the \textit{intent} of the utterance (see table \ref{tab:nlu}). Slots of a frame, which augment user intents, are referred to as \textit{entities} and can take on a predefined set of values.

\begin{table}[!htbp]
	\begin{center}
			\begin{tabularx}{\textwidth}{ c |  *{6}{|Y} }
				\hline
				\textbf{Utterance} & book & flight & from & Berlin & to & Munich \\ \hline
				\textbf{Entities} &  &  &  & \texttt{depart.} &  & \texttt{dest.} \\ \hline
				\textbf{Intent} & \multicolumn{6}{c}{\texttt{book\_flight}} \\
				\hline
			\end{tabularx}
	\end{center}
	\vspace{-15pt}
	\caption{An user utterance represented by intent and entities.} \label{tab:nlu}
\end{table}

\colorbox{yellow}{Strategy, actions, user stories...}

\colorbox{yellow}{Transition to pipelined systems...}

\input{chapters/_fundamentals_pipelinedsystems}
\input{chapters/_fundamentals_endtoendsystems}

\section{Sequence Modeling With Neural Networks}

\colorbox{yellow}{Why is sequence modeling necessary?}

This section shall serve as a basic introduction to sequence modeling techniques for neural networks. For a detailed exposition of preliminary concepts, including deep feedforward networks and gradient-based optimization algorithms such as back-propagation, the reader may refer to Goodfellow et al. \cite{goodfellow2016}

\input{chapters/_fundamentals_recurrentneuralnetworks}
\input{chapters/_fundamentals_attentionmechanism}
\input{chapters/_fundamentals_memorynetworks}
\input{chapters/_fundamentals_hybridcode}