\subsection{Pipelined Dialogue Systems}

Dialogue systems, as well as the domain ontologies and strategies they implement, are often realized in a modular way where each component corresponds to a particular step in a processing pipeline (see figure \ref{fig:capipeline}). The Natural Language Understanding (NLU) component is responsible for transforming a raw natural language audio clip or text string into an structured format, containing intents and entities, usable for further processing. Next, given the input user utterance and the extracted intents and entities and the current state of the dialogue, a dialogue state tracker estimates the next dialogue state. This provides contextual awareness to the conversational agent, as the dialogue state encodes information mentioned in previous dialogue turns. Based on the most recent state estimate a dialogue policy determines the most appropriate machine response. Finally, a Natural Language Generation module transforms a structured machine response back into natural language string.

\begin{figure}[h!]
  \centering
		\includegraphics[width=\linewidth]{images/ca_pipeline.png}
  \caption{Dialogue system pipeline model.}
  \label{fig:capipeline}
\end{figure}

\subsubsection*{Natural Language Understanding}

This module is concerned with inferring meaning from user utterances. It involves semantic analysis for obtaining a structured representation of the input. The resulting format is usually comprised of \textit{intents} and \textit{entities}. Given an input user utterance, intent detection classifies its meaning based on a set of predefined intents, representing the most likely user objective. Entities modify intents and are obtained by extracting additional word-level information from an user utterance \colorbox{yellow}{[CITE]}. This is usually achieved by tagging word sequences with slots contained in the slot frame the intent invokes (see table \ref{tab:nlu}). This process is referred to as \textbf{semantic parsing}. The common approach to semantic parsing is to derive various candidate structured meaning representations using grammar formalisms such as Combinatory Categorial Grammars \cite{artzi2013}. Subsequently, a probability distribution over the resulting representations is produced using statistical methods such as log-linear models \cite{pasupat2015}. The highest scoring candidate representations are chosen and passed to to the next processing stage (see table \ref{tab:meaningrep}).

\colorbox{yellow}{Fix table listing.}

\begin{table}[!htbp]
	\vspace{5pt}
	\begin{small}
	\begin{center}
			\begin{tabular}{ c | l  l }
			\hline
				Input Utterance & \multicolumn{2}{p{8cm}}{\enquote{Please book a flight from Berlin to Munich.}} \\ \hline
				Meaning Representation & \multicolumn{2}{p{8cm}}{\texttt{\{ intent = 'book\_flight', entities = [ departure='BER', destination='MUC' ] \}}} \\ \hline
			\end{tabular}
	\end{center}
	\end{small}
	\vspace{-15pt}
	\caption{Meaning representation of a natural language query.} 
	\label{tab:meaningrep}
\end{table}

\subsubsection*{Dialogue State Tracking}

Using state machines for managing conversation states proves to be complicated and error-prone even for small conversation domains \colorbox{yellow}{[CITE]}. Thus, dialogue state trackers manage the conversation state by filling and updating the slot frame \colorbox{yellow}{[CITE]}. This provides contextual awareness and enables a dialogue system to resolve user utterances for entities mentioned in previous dialogue turns . In Figure \ref{fig:caconv}, for example, the user does not have to repeat that she is looking for a \textit{cheap} italian restaurant, since the dialogue state tracker keeps a record of slot values from previous dialogue turns. However, the user may change her mind and ask for a mid-range restaurant. In that case, the dialogue state tracker is responsible for updating corresponding slots. The current dialogue state is predicted using both the current input from the NLU module and some record of the dialogue history and previous state estimates. Usually, using these pieces of information, the dialogue state tracker employs a statistical model to estimate a probability distribution over each slot to determine the appropriate value for each dialogue turn. The obtainted dialogue state $H_t$ at time step $t$ is referred to as \textit{belief state} of the system \colorbox{yellow}{[CITE]}. Different approaches to estimating the next belief state have been employed, such as Partially Observable Markov Decision Processes \colorbox{yellow}{[CITE]} or Conditional Random Fields \colorbox{yellow}{[CITE]}. Deep learning methods have also been employed in dialogue state tracking. \colorbox{yellow}{[CITE]} propose a multi-domain recurrent neural network approach for predicting belief states.

\subsubsection*{Dialogue Policy}

The dialogue policy modules input is the current belief state $H_t$. It uses the belief state to produce the next system action. In Figure \ref{fig:caconv}, for example, since the type of cuisine was not specified, the selected system action was to ask for that missing piece of information. Data-driven approaches to implementing dialogue policies have become the state-of-the-art and have replacced rule-based approaches. \colorbox{yellow}{Examples}

\begin{table}[!htbp]
	\vspace{5pt}
	\begin{center}
			\begin{tabularx}{0.8\textwidth}{ c | c }
				\textbf{Dialogue act} & \textbf{Description} \\ \hline \hline
				\texttt{hello(a=x, b=y, ...)} & Open dialogue and give info a=x, b=y, ... \\ \hline
				\texttt{inform(a=x, b=y, ...)} & Give information a=x, b=y, ... \\ \hline
				\texttt{request(a, b=x, ...)} & Request value for a given b=x, ... \\ \hline
				\texttt{confirm(a=x, b=y, ...)} & Explicitly confirm a=x, b=y, ... \\ \hline
				\texttt{select(a=x, a=y, ...)} & Select between a=x, a=y, ... \\ \hline
				... & ... 
			\end{tabularx}
	\end{center}
	\vspace{-15pt}
	\caption{Common dialogue acts \colorbox{yellow}{...}} \label{tab:act}
\end{table}

\subsubsection*{Natural Language Generation}

Using the natural language generation (NLG) module, a system action and the corresponding belief state are translated back into an natural language utterance. Thus, it can be viewed as performing the opposite operation to the NLU module. NLG is usually divided into text planning, sentence planning and surface realization \colorbox{yellow}{[CITE]]}. \colorbox{yellow}{text planning} \colorbox{yellow}{sentence planning} Using the abstract representation of the natural language text, surface realization employs grammar rules to produce the actual text.. Deep learning techniques have also been widely employed in natural language generation. For example, Hu et al. \colorbox{yellow}{[CITE]} propose variational autoencoders. Du\v{s}ek and Jurcicek propose a sequence-to-sequence model with attention (see section \colorbox{yellow}{[REF]}).